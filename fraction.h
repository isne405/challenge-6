#include<iostream>
#ifndef FRACTION_H
#define FRACTION_H

using namespace std;



class fraction	{
	public:
		fraction();
		fraction(int newnumer);
		fraction(int newnumer ,int newdenum);
		void set_numer(int newnumer); //Mutator
		void set_denum(int newdenum);
		int getNumer();//Accessor
		int getDenum();
		
		friend fraction operator *(fraction f1 , fraction f2); // operator *
		friend fraction operator / (fraction f1 , fraction f2); //operator /
		friend fraction operator +(fraction f1 , fraction f2); // operator +
		friend fraction operator -(fraction f1 , fraction f2); // operator -
		friend fraction operator ++(fraction &f1);//operator ++
		friend fraction operator --(fraction &f1);//operator --
		friend ostream &operator <<(ostream &os,fraction f3); // operator <<
		friend istream &operator >>(istream in , fraction f3); // function >>
		friend bool operator ==(fraction , fraction); // operator ==
		friend bool operator !=(fraction , fraction ); // operator !=
		friend bool operator >(fraction f1 , fraction f2);// operator >
		friend bool operator >=(fraction f1 , fraction f2);// operator >=
		friend bool operator <(fraction f1 , fraction f2);// operator <
		friend bool operator <=(fraction f1 , fraction f2);//operator <=
	
	private:
		int numer;
		int denum;
};

#endif //FRACTION_H