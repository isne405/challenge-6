#include "fraction.h"
#include <iostream>
using namespace std;

fraction::fraction(){ //constrctor case 1
	
}

fraction::fraction(int newnumer){ //constructor case 2
	fraction::set_numer(numer);
	fraction::set_denum(1);
}

fraction::fraction(int newnumer , int newdenum){ //constructor case 3
	fraction::set_numer(newnumer);
	fraction::set_denum(newdenum);
} 

void fraction::set_numer(int newnumer){ //Mutator
	numer = newnumer;
}

void fraction::set_denum(int newdenum){
	denum = newdenum;
}

int fraction::getNumer(){ //Accessor
	return numer;
}

int fraction::getDenum(){
	return denum;
}

fraction operator +(fraction f1 , fraction f2){
	fraction result;
	if(f1.denum == f2.denum){
		result.set_numer(f1.numer + f2.numer);
		result.set_denum(f1.denum);
	}else if(f1.denum != f2.denum){
		result.set_denum(f1.denum * f2.denum);
		result.set_numer((f1.numer * f2.denum) + (f2.numer * f1.denum));
	}
	return result;
}

fraction operator -(fraction f1 , fraction f2){
		fraction result;
	if(f1.denum == f2.denum){
		result.set_numer(f1.numer - f2.numer);
		result.set_denum(f1.denum);
	}else if(f1.denum != f2.denum){
		result.set_denum(f1.denum * f2.denum);
		result.set_numer((f1.numer * f2.denum) - (f2.numer * f1.denum));
	}
	return result;
}

fraction operator *(fraction f1 , fraction f2){
	fraction result;
			
	result.set_numer(f1.numer * f2.numer);
	result.set_denum(f1.denum * f2.denum);
		
	return result;
} 

fraction operator /(fraction f1 , fraction f2){
		fraction result;
			
		result.set_numer(f1.numer * f2.denum);
		result.set_denum(f1.denum * f2.numer);
		
		return result;
}

fraction operator ++(fraction &f1){
	fraction result;
	f1.numer = f1.numer + f1.denum;
	result.set_numer(f1.numer);
	result.set_denum(f1.denum);

	return result;
}

fraction operator --(fraction &f1){
	fraction result;
	int newnumer;
	f1.numer = f1.numer - f1.denum;
	result.set_numer(f1.numer);
	result.set_denum(f1.denum);

	return result;
}

bool operator ==(fraction f1 ,fraction f2){
	if(f1.numer == f2.numer && f1.denum == f2.denum){
		return true;
	}else{
		return false;
	}
}

bool operator !=(fraction f1 ,fraction f2){
	if(f1.numer != f2.numer && f1.denum != f2.denum){
		return true;
	}else{
		return false;
	}
}

bool operator >(fraction f1 ,fraction f2){
	if((f1.numer / f1.denum) > (f2.numer / f2.denum)){
		return true;
	}else{
		return false;
	}
}

bool operator >=(fraction f1 ,fraction f2){
	if((f1.numer / f1.denum) > (f2.numer / f2.denum) || (f1.numer / f1.denum) == (f2.numer / f2.denum)){
		return true;
	}else{
		return false;
	}
}

bool operator <(fraction f1 ,fraction f2){
	if((f1.numer / f1.denum) < (f2.numer / f2.denum)){
		return true;
	}else{
		return false;
	}
}

bool operator <=(fraction f1 ,fraction f2){
	if((f1.numer / f1.denum) < (f2.numer / f2.denum) || (f1.numer / f1.denum) == (f2.numer / f2.denum)){
		return true;
	}else{
		return false;
	}
}

ostream &operator <<(ostream &out , fraction f1){
	out << f1.numer << "/" << f1.denum;
	return out;
}

/*istream &operator >>(istream &in , fraction f1){
	int temp1 , temp2;
	cout << "Numerator : ";
	cin >> temp1;
	f1.set_numer(temp1);
	return temp1;
	cout << "Denominator : ";
	cin >> temp2;
	f1.set_denum(temp2);
	return temp2;
}*/