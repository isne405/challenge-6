#include<iostream>
#include"fraction.h"

using namespace std;

void main(){

	fraction f1(3,5);
	fraction f2(4,7);
	fraction f3;
	fraction f4(8,9);
	f3 = f1 + f2;
	cout <<"+ operator" << f3 << endl;
	f3=0;
	f3 = f1-f2;
	cout <<"- operator " << f3 << endl;
	f3 = 0;
	f3 = f1*f2;
	cout <<"* operator " << f3 << endl;
	f3 = 0;
	f3 = f1/f2;
	cout <<"/ operator " << f3 << endl;
	f3++;
	cout <<"++ operator " << f3 << endl;
	f3--;
	cout <<"-- operator " << f3 << endl;

	if(f3 > f4 && f3 >= f4){
		cout << "true : f3 > f4" << endl;
	}
	if(f3 < f4 && f3 <= f4){
		cout << "true : f3 < f4" << endl;
	}else{cout << "flase , f3 > f4" << endl;}
	if(!(f3 > f4 && f3 <= f4)){
		cout << "false" << endl;
	}
	if(!(f3 < f4 && f3 >= f4)){
		cout << "flase" << endl;
	}
	



}